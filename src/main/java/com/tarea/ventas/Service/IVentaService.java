package com.tarea.ventas.Service;

import com.tarea.ventas.model.Venta;

public interface IVentaService extends ICRUD<Venta, Integer>{
    Venta registrarTransaccion(Venta venta)throws Exception;
}
