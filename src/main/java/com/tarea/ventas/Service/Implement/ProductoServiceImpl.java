package com.tarea.ventas.Service.Implement;

import com.tarea.ventas.Service.IProductoService;
import com.tarea.ventas.model.Producto;
import com.tarea.ventas.repository.IGenericRepository;
import com.tarea.ventas.repository.IProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductoServiceImpl extends CRUDImplement<Producto, Integer> implements IProductoService {
    @Autowired
    private IProductoRepository repo;

    @Override
    protected IGenericRepository<Producto, Integer> getRepo() {
        return repo;
    }
}
