package com.tarea.ventas.Service.Implement;

import com.tarea.ventas.Service.IVentaService;
import com.tarea.ventas.model.Venta;
import com.tarea.ventas.repository.IGenericRepository;
import com.tarea.ventas.repository.IVentaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class VentaServiceImpl extends CRUDImplement<Venta,Integer> implements IVentaService {
    @Autowired
    private IVentaRepository repo;

    @Override
    protected IGenericRepository<Venta, Integer> getRepo() {
        return repo;
    }

    @Transactional
    @Override
    public Venta registrarTransaccion(Venta venta) throws Exception {
        venta.getDetalleVenta().forEach(det -> det.setIdVenta(venta));
        repo.save(venta);
        return venta;
    }
}
