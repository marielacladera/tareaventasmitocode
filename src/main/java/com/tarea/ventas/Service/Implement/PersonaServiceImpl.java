package com.tarea.ventas.Service.Implement;

import com.tarea.ventas.Service.IPersonaService;
import com.tarea.ventas.model.Persona;
import com.tarea.ventas.repository.IGenericRepository;
import com.tarea.ventas.repository.IPersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonaServiceImpl extends CRUDImplement<Persona, Integer> implements IPersonaService {
    @Autowired
    private IPersonaRepository repo;

    @Override
    protected IGenericRepository<Persona, Integer> getRepo() {
        return repo;
    }
}
