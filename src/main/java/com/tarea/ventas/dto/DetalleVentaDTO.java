package com.tarea.ventas.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotNull;

public class DetalleVentaDTO {
    private Integer idDetalle;

    @JsonIgnore
    private VentaDTO idVenta;

    @NotNull
    private Integer cantidad;

    @NotNull
    private ProductoDTO producto;

    public Integer getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(Integer idDetalle) {
        this.idDetalle = idDetalle;
    }

    public VentaDTO getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(VentaDTO idVenta) {
        this.idVenta = idVenta;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public ProductoDTO getProducto() {
        return producto;
    }

    public void setProducto(ProductoDTO producto) {
        this.producto = producto;
    }
}
