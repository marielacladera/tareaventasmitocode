package com.tarea.ventas.dto;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

public class VentaDTO {
    private Integer idVenta;

    @NotNull
    private LocalDateTime fecha;

    @NotNull
    private Double importe;

    @NotNull
    private PersonaDTO persona;

    @NotNull
    private List<DetalleVentaDTO> detalleVenta;

    public Integer getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Integer idVenta) {
        this.idVenta = idVenta;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }

    public PersonaDTO getPersona() {
        return persona;
    }

    public void setpersona(PersonaDTO persona) {
        this.persona = persona;
    }

    public List<DetalleVentaDTO> getDetalleVenta() {
        return detalleVenta;
    }

    public void setDetalleVenta(List<DetalleVentaDTO> detalleVenta) {
        this.detalleVenta = detalleVenta;
    }
}
