package com.tarea.ventas.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Size;

@Schema(description = "Persona DTO Data")
public class PersonaDTO {
    private Integer idPersona;

    @Schema(description = "nombres de la persona")
    @Size(min = 3, max = 70, message = "{nombres.size}")
    private String nombres;

    @Schema(description = "apellidos de la persona")
    @Size(min = 3, max = 70, message = "{nombres.size}")
    private String apellidos;

    @Schema(description = "ci de la persona")
    @Size(min = 5, max = 17, message = "{ci.size}")
    private String ci;

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }
}
