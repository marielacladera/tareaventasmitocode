package com.tarea.ventas.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Size;

@Schema(description = "Producto DTO Data")
public class ProductoDTO {
    private Integer idProducto;

    @Schema(description = "nombre del producto")
    @Size(min = 3, max = 70, message = "{nombreProducto.size}")
    private String nombre;

    @Schema(description = "nombre de la marca del producto")
    @Size(min = 3, max = 70, message = "{marca.size}")
    private String marca;


    @Schema(description = "precio del producto")
    private Double precio;

    @Schema(description = "descripcion del producto")
    @Size(max = 100, message = "{descripcion.size}")
    private String descripcion;

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
