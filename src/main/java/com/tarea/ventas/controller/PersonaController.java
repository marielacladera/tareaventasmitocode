package com.tarea.ventas.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.tarea.ventas.Service.IPersonaService;
import com.tarea.ventas.dto.PersonaDTO;
import com.tarea.ventas.exception.ModeloNotFoundException;
import com.tarea.ventas.model.Persona;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/personas")
public class PersonaController {
    @Autowired
    private IPersonaService service;

    @Autowired
    private ModelMapper mapper;

    @GetMapping
    public ResponseEntity<List<PersonaDTO>> listar() throws Exception{
        List<PersonaDTO> lista = service.listar().stream().map(es->mapper.map(es,PersonaDTO.class)).collect(Collectors.toList());
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<PersonaDTO> registrar(@Valid @RequestBody PersonaDTO dtoRequest) throws Exception{
        Persona per = mapper.map(dtoRequest, Persona.class);
        Persona obj = service.registrar(per);
        PersonaDTO dtoResponse = mapper.map(obj, PersonaDTO.class);
        return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<PersonaDTO> modificar(@Valid @RequestBody PersonaDTO dtoRequest) throws Exception{
        Persona per = service.listarPorID(dtoRequest.getIdPersona());
        if(per == null){
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdPersona());
        }
        Persona pers = mapper.map(dtoRequest, Persona.class);
        Persona obj = service.modificar(pers);
        PersonaDTO dtoResponse = mapper.map(obj, PersonaDTO.class);
        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminar (@PathVariable("id") Integer id) throws  Exception{
        Persona per = service.listarPorID(id);
        if(per == null){
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
        }
            service.eliminar(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonaDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
        PersonaDTO dtoResponse;
        Persona per = service.listarPorID(id);
        if(per == null){
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
        }else{
            dtoResponse = mapper.map(per, PersonaDTO.class);
        }
        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }
    @GetMapping("/hateoas/{id}")
    public EntityModel<PersonaDTO> listarHateoas(@PathVariable("id") Integer id) throws Exception{
        PersonaDTO dto;
        Persona obj = service.listarPorID(id);
        if(obj == null){
            throw new ModeloNotFoundException("ID NO ENCONTRADO" + id);
        }
        dto = mapper.map(obj, PersonaDTO.class);
        EntityModel<PersonaDTO>  recurso = EntityModel.of(dto);
        WebMvcLinkBuilder list1 = linkTo(methodOn(this.getClass()).listarPorId(id));
        recurso.add(list1.withRel("persona-link"));
        return recurso;
    }
}
