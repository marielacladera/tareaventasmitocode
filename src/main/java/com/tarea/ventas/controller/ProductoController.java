package com.tarea.ventas.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.tarea.ventas.Service.IProductoService;
import com.tarea.ventas.dto.ProductoDTO;
import com.tarea.ventas.exception.ModeloNotFoundException;
import com.tarea.ventas.model.Producto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/productos")
public class ProductoController {
    @Autowired
    private IProductoService service;

    @Autowired
    private ModelMapper mapper;

    @GetMapping
    public ResponseEntity<List<ProductoDTO>> listar() throws Exception{
        List<ProductoDTO> lista = service.listar().stream().map(pr->mapper.map(pr,ProductoDTO.class)).collect(Collectors.toList());
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    /*@PostMapping
    public ResponseEntity<ProductoDTO> registrar(@Valid @RequestBody ProductoDTO dtoRequest) throws Exception{
        Producto pr = mapper.map(dtoRequest, Producto.class);
        Producto obj = service.registrar(pr);
        ProductoDTO dtoResponse = mapper.map(obj, ProductoDTO.class);
        URI location  = ServletUriComponentsBuilder.fromCurrentRequest().path(("{/id}")).buildAndExpand(dtoResponse.getIdProducto()).toUri();
        return ResponseEntity.created(location).build();
    }*/

    @PostMapping
    public ResponseEntity<ProductoDTO> registrar(@Valid @RequestBody ProductoDTO dtoRequest) throws Exception{
        Producto pr = mapper.map(dtoRequest, Producto.class);
        Producto obj = service.registrar(pr);
        ProductoDTO dtoResponse = mapper.map(obj, ProductoDTO.class);
        return new ResponseEntity<>(dtoResponse, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<ProductoDTO> modificar(@Valid @RequestBody ProductoDTO dtoRequest) throws Exception{
        Producto pr = service.listarPorID(dtoRequest.getIdProducto());
        if(pr == null){
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdProducto());
        }
        Producto prd = mapper.map(dtoRequest, Producto.class);
        Producto obj = service.modificar(prd);
        ProductoDTO dtoResponse = mapper.map(obj, ProductoDTO.class);
        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminar (@PathVariable("id") Integer id) throws  Exception{
        Producto pr = service.listarPorID(id);
        if(pr == null){
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
        }
        service.eliminar(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductoDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
        ProductoDTO dtoResponse;
        Producto pr = service.listarPorID(id);
        if(pr == null){
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
        }else{
            dtoResponse = mapper.map(pr, ProductoDTO.class);
        }
        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }
    @GetMapping("/hateoas/{id}")
    public EntityModel<ProductoDTO> listarHateoas(@PathVariable("id") Integer id)throws Exception{
        Producto obj = service.listarPorID(id);
        if(obj == null){
            throw  new ModeloNotFoundException("ID NO ENCONTRADO" + id);
        }
        ProductoDTO dto = mapper.map(obj, ProductoDTO.class);
        EntityModel<ProductoDTO> recurso = EntityModel.of(dto);
        WebMvcLinkBuilder link1 = linkTo(methodOn(this.getClass()).listarPorId(id));
        recurso.add(link1.withRel("producto-link"));
        return recurso;
    }
}