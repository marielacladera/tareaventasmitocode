package com.tarea.ventas.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.tarea.ventas.Service.IVentaService;
import com.tarea.ventas.dto.VentaDTO;
import com.tarea.ventas.exception.ModeloNotFoundException;
import com.tarea.ventas.model.Venta;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/ventas")
public class VentaController {
    @Autowired
    private IVentaService service;

    @Autowired
    private ModelMapper mapper;

    @GetMapping
    public ResponseEntity<List<VentaDTO>> listar() throws Exception{
        List<VentaDTO> lista = service.listar().stream().map(ven->mapper.map(ven, VentaDTO.class)).collect(Collectors.toList());
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Void> registrar(@Valid @RequestBody VentaDTO dtoRequest) throws Exception {
        Venta ven = mapper.map(dtoRequest, Venta.class);
        Venta obj = service.registrarTransaccion(ven);
        VentaDTO dtoResponse= mapper.map(obj, VentaDTO.class);
        URI location  = ServletUriComponentsBuilder.fromCurrentRequest().path("{/id}").buildAndExpand(dtoResponse.getIdVenta()).toUri();
        return ResponseEntity.created(location).build();
        //return new ResponseEntity<>(dtoRequest, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<VentaDTO> modificar(@Valid @RequestBody VentaDTO dtoRequest) throws Exception{
        Venta ven = service.listarPorID(dtoRequest.getIdVenta());
        if(ven == null){
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + dtoRequest.getIdVenta());
        }
        Venta ve = mapper.map(dtoRequest, Venta.class);
        Venta obj = service.modificar(ve);
        VentaDTO dtoResponse = mapper.map(obj, VentaDTO.class);
        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminar (@PathVariable("id") Integer id) throws  Exception{
        Venta ven = service.listarPorID(id);
        if(ven == null){
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
        }
        service.eliminar(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    public ResponseEntity<VentaDTO> listarPorId(@PathVariable("id") Integer id) throws Exception{
        VentaDTO dtoResponse;
        Venta ven = service.listarPorID(id);
        if(ven == null){
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
        }else{
            dtoResponse = mapper.map(ven, VentaDTO.class);
        }
        return new ResponseEntity<>(dtoResponse, HttpStatus.OK);
    }

    @GetMapping("/hateoas/{id}")
    public EntityModel<VentaDTO> listarHateoas(@PathVariable("id") Integer id) throws Exception{
        VentaDTO dto;
        Venta obj = service.listarPorID(id);
        if(obj == null){
            throw new ModeloNotFoundException("ID NO ENCONTRADO" + id);
        }
        dto = mapper.map(obj, VentaDTO.class);
        EntityModel<VentaDTO>  recurso = EntityModel.of(dto);
        WebMvcLinkBuilder list1 = linkTo(methodOn(this.getClass()).listarPorId(id));
        recurso.add(list1.withRel("venta-link"));
        return recurso;
    }
}

