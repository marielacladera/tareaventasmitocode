package com.tarea.ventas.repository;

import com.tarea.ventas.model.Producto;

public interface IProductoRepository extends IGenericRepository<Producto, Integer> {
}
