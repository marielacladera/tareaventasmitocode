package com.tarea.ventas.repository;

import com.tarea.ventas.model.DetalleVenta;

public interface IDetalleVentaRepository extends IGenericRepository<DetalleVenta, Integer>{
}
