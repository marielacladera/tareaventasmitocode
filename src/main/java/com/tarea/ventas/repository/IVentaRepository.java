package com.tarea.ventas.repository;

import com.tarea.ventas.model.Venta;

public interface IVentaRepository extends IGenericRepository<Venta, Integer>{
}
