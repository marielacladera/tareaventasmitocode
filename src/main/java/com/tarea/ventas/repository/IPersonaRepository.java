package com.tarea.ventas.repository;

import com.tarea.ventas.model.Persona;

public interface IPersonaRepository extends IGenericRepository<Persona, Integer> {
}
